package com.spring.security.demo.model;

public class DemoResponse {

	private String message;
	private String authType;
	private String authKey;
	
	public DemoResponse(String message, String authType, String authKey) {
		super();
		this.message = message;
		this.authType = authType;
		this.authKey = authKey;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	
}
