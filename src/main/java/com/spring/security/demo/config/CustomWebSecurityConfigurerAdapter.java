package com.spring.security.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class CustomWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter{
	 
		/*-@Autowired
	    private MyBasicAuthenticationEntryPoint authenticationEntryPoint;*/
	
		private static String[] noAuthEndpoints = {"/","/no-auth"};
		private static String[] basicAuthEndpoints = {"/basic-auth"};
		
	 
	    @Autowired
	    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
	        auth.inMemoryAuthentication()
	          .withUser("admin").password(passwordEncoder().encode("admin"))
	          .authorities("ROLE_ADMIN");
	    }
	 
	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http.csrf().disable().authorizeRequests()
	          .antMatchers(noAuthEndpoints).permitAll() 
	          .antMatchers(basicAuthEndpoints).authenticated().and().httpBasic() /*Use anyRequest() for all remaining request*/
	          .and()
	          .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);;
	          //.authenticationEntryPoint(authenticationEntryPoint);
	 
	       /*- http.addFilterAfter(new CustomFilter(),
	          BasicAuthenticationFilter.class);*/
	    }
	 
	    @Bean
	    public PasswordEncoder passwordEncoder() {
	        return new BCryptPasswordEncoder();
	    }

}
