package com.spring.security.demo.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.spring.security.demo.model.DemoResponse;

@RestController
public class BasicAuthController {
	
	@GetMapping(path="/basic-auth", produces=MediaType.APPLICATION_JSON_VALUE)
	public DemoResponse getWelcomeMessage(@RequestHeader String authorization){
		return new DemoResponse("Welcome. This is base url.",
				"Basic Auth",
				authorization);
	}
}
