package com.spring.security.demo.controller;

import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.security.demo.model.DemoResponse;

@RestController
public class NoAuthController {
	
	@GetMapping(path="/no-auth", produces=MediaType.APPLICATION_JSON_VALUE)
	public DemoResponse getWelcomeMessage(){
		return new DemoResponse("Welcome. This is context path with no authentication.",
				"None",
				"N/A");
	}
}
