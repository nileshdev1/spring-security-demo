########## Build stage - If required uncommnet
#FROM maven:3.6.0-jdk-8-slim AS build
#COPY src /home/app/src
#COPY pom.xml /home/app
#RUN mvn -f /home/app/pom.xml clean package

# For Java 8, try this
FROM openjdk:8-jdk-alpine

# For Java 11, try this
#FROM adoptopenjdk/openjdk11:alpine-jre

# Refer to Maven build -> finalName
ARG JAR_FILE=target/spring-security-demo-2020.09.0.1.jar

# Set Work Dir
WORKDIR /opt/app/

# Copy boot jar
COPY ${JAR_FILE} spring-security-demo.jar

#Example for RUN command
#RUN mkdir -p /buildInfo/
#RUN echo "Build Time: $(date)" >> info.txt
#RUN useradd -ms /bin/bash nilesh
#USER nilesh
#Below is example to do run operation in single layer
RUN mkdir -p /buildInfo/ && \
	echo "Build Time: $(date)" >> /buildInfo/info.txt && \
	apk add --update bash curl
	
#apt-get update && apt-get install -y curl bash
	
# Start Application
ENTRYPOINT ["java","-jar","spring-security-demo.jar"]